global class InboundExample implements Messaging.InboundEmailHandler{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope){
        Messaging.InboundEmailResult result=new Messaging.InboundEmailResult();
        try{
            Account a=new Account();
            a.Name=email.fromName;
            a.description=email.plainTextBody;
            insert a;
            result.success=true;
        }
        catch(Exception e){
           result.success=false; 	
        }
        return result;
    }

}