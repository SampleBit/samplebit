public class AttachmentGenerator {
	
	private  Account a; //Account object
	
	//constructor
	public AttachmentGenerator(ApexPages.StandardController standardPageController) {
		a = (Account)standardPageController.getRecord(); //instantiate the Account object for the current record
        a = [select Account.OwnerId,Account.Owner.Email from Account limit 1];
	}
	
	//method called from the Visualforce's action attribute
	public PageReference attachPDF() {
		//generate and attach the PDF document
		PageReference pdfPage = Page.DocPDF; //create a page reference to our pdfDemo Visualforce page, which was created from the post http://www.interactiveties.com/blog/2015/render-visualforce-pdf.php
		Blob pdfBlob = pdfPage.getContent(); //get the output of the page, as displayed to a user in a browser
		Attachment attach = new Attachment(parentId = a.Id, Name = 'pdfAttachmentDemo.pdf', body = pdfBlob); //create the attachment object
		insert attach; //insert the attachment
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        
       User user=[select id,name,Email from User where id=:a.OwnerId];
        String[] toAddresses = new String[]{user.Email};
       
       Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('attachment.pdf');
        efa.setContentType('application/pdf');
        efa.setBody(pdfBlob);
       
        email.setSubject('Testing PDF attachment');
       
        email.setToAddresses(toAddresses);
        email.setSenderDisplayName('Salesforce');
        email.setPlainTextBody('Hi ' +user.name+ ',   Please find the below Invoice Attachment ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        // Sends the email 
   
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
        System.debug('>>>>>Email sent result'+r);
       // return null;
		//redirect the user
		PageReference pageWhereWeWantToGo = new ApexPages.StandardController(a).view(); //we want to redirect the User back to the Account detail page
		pageWhereWeWantToGo.setRedirect(true); //indicate that the redirect should be performed on the client side
		return pageWhereWeWantToGo; //send the User on their way
        
	
    }
}