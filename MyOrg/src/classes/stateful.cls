global class stateful implements Database.Batchable<Sobject>,Database.Stateful{
    public static Integer count=0;
    public Integer size=0;
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('select name,industry from Account');
    }
    global void execute(Database.BatchableContext bc,List<Account>scope){
        for(Account a:scope){
            count=count+1;
            size=size+1;
        }   
    }
    global void finish(Database.BatchableContext bc){
        Account a=new Account(Name='count'+count+ 'size'+size);
        insert a;
    }

}