public class Nmultiselect {
	public List<String> cities{set;get;}
	public Set<String> nscities{set;get;}
	public set<String> scities{set;get;}
	public List<SelectOption> nsoptions{set;get;}
	public List<SelectOption> soptions{set;get;}
	public List<String> selected{set;get;}
	public List<String> removed{Set;get;}
	public Nmultiselect(){
		cities=new List<String>{'Hyd','Ban','Che','Pune'};
		nscities=new Set<String>();
		scities=new  Set<String>();
		nscities.addAll(cities);
		 nsoptions=new List<SelectOption>();
		 soptions=new List<selectOption>();
		 selectOption op=new SelectOption('none','-None-');
		 soptions.add(op);
		 createList();
	}
	public void createList(){
		nsoptions.clear();
		soptions.clear();
		 selectOption op=new SelectOption('none','-None-');
		 soptions.add(op);
		 nsoptions.add(op);
		for(String s1: nscities){
			Selectoption op1=new SelectOption(s1,s1);
			nsoptions.add(op1);
		}
		for(String s2: scities){
			Selectoption op2=new SelectOption(s2,s2);
			soptions.add(op2);
		}
	}
	public void addEle(){
		nscities.removeAll(selected);
		scities.addAll(selected);
		createList();
	}
	public void removeEle(){
		scities.removeAll(removed);
		nscities.addAll(removed);
		createList();
	}	
}