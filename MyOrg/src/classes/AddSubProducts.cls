public class AddSubProducts {
    public List<ProductLineItem__c> pitem{get;set;}//For Machinary Products
     

     public List<QuoteLine__c> qitem{get;set;}
     public List<QuoteProductLineItem__c> qlitem{get;set;}   
     public ID qid {get; set;}
     
    
     public string searchText {get;set;}
     public String message {get;set;}
    public string searchString {get;set;}
     
     public AddSubProducts(ApexPages.StandardController controller){
         pitem = new List<ProductLineItem__c>();
         
         qitem = new List<QuoteLine__c>();
         qlitem = new List<QuoteProductLineItem__c>();
        qid = ApexPages.currentPage().getParameters().get('qid');
         
        /* pitem = [select id,name,Applicable__c ,M_Products__c ,Catalogue_Price__c,Final_Selling_Price__c,Minimum_Selling_Price__c
                   from Product_Line_Items__c ];*/
      
         qitem = [SELECT id,Name, Company_Name__c,CProduct__r.Name FROM QuoteLine__c where id=:qid];
         for(QuoteLine__c qli:qitem){
         pitem=[select id,name,Applicable__c,CProduct__r.Name  from ProductLineItem__c where CProduct__r.Name=:qli.Name];
         
         searchText = System.currentPageReference().getParameters().get('lksrch');
         searchString = System.currentPageReference().getParameters().get('lksrch');
     }
    }
     
   
    public PageReference search()
         {
             runSearch(); 
               return null;
          }
     private void runSearch() {
         
             pitem = performSearch(searchString);   
             
          } 

     private List<ProductLineItem__c> performSearch(string searchString) {

         String soql = 'select id,name,Applicable__c,CProducts__r.Name from ProductLineItem__c';
           if(searchString != '' && searchString != null ) 
           soql = soql +  ' where Name LIKE \'%' + searchString +'%\'';
           soql = soql + ' limit 25';
           System.debug(soql);
           return database.query(soql); 

        }
    public string getFormTag() {
         return System.currentPageReference().getParameters().get('frm');
      }

    
    Public  PageReference csave()
    {
       qid = ApexPages.currentPage().getParameters().get('qid');
       try 
       {
          for(ProductLineItem__c p:pitem)
          {
              QuoteProductLineItem__c q = new QuoteProductLineItem__c();
              if(p.Applicable__c==TRUE)
              {
                   
                   q.QuoteLine__c =qid; 
                   q.Name =p.Name;
                   
                   
                  
                   
                   qlitem.add(q); 
                  
              }
            
          }
           
           system.debug('&&&&'+qid);
           Insert qlitem;
           message = 'updated records Sucessfully';
        } 
        Catch (DMLException e) 
        {
            ApexPages.addMessages(e);
            return null;
        }
        
        //return new PageReference('javascript:self.close()');
        return new PageReference('/'+qid);
   }
   public PageReference cancel() 
   {
    return new PageReference('/'+ApexPages.currentPage().getParameters().get('qid'));
   }

}