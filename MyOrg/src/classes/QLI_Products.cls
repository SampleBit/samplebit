public with sharing class QLI_Products {
    
  
   public id oppid;
   public List<QuoteLine__c>  Md{get;set;}
      
    public QLI_Products(ApexPages.StandardController controller) {

     
          oppid=Apexpages.currentPage().getParameters().get('qid');
           Md=new List<QuoteLine__c> ();
          
           QuoteLine__c  m=new QuoteLine__c();
           m.MyQuote__c=oppid;
           Md.add(m);
           
           
    }
    
   
   
      public void addrow()
      {
        QuoteLine__c m=new QuoteLine__c();
           m.MyQuote__c=oppid;
           Md.add(m);
       }
               
       public void removerow()
        {
             Integer i = Md.size(); 
             Md.remove(i-1);
         }
    
     public pagereference save() 
     {
        insert Md;
         PageReference p=new PageReference('/'+ oppid + '?nooverride=1');
       //PageReference p=new PageReference('/apex/QuotePDF');
         p.getParameters().put('Id', oppid);
         p.setRedirect(true);
         return p;
     /*   PageReference home = new PageReference('/' + oppid + '?nooverride=1');
        home.setRedirect(true);
        return home;*/
         
     }
     public pagereference cancel()
     {
         PageReference home = new PageReference('/' + oppid + '?nooverride=1');
         home.setRedirect(true);
         return home;
     }
    public void autoprice()
    {
        for(QuoteLine__c op:Md)
        {
          if(String.isNotBlank(op.CProduct__c))
          {
            for(CProduct__c pro : [SELECT Name,Id,Company_Name__c,Applicable__c,Unit_Price__c,Minimum_Selling_Price__c  FROM CProduct__c where id=:op.CProduct__c])
            {
        
              // op.CProduct__r.name=pro.Name;
               op.Company_Name__c=pro.Company_Name__c;
               op.Unit_Price__c=pro.Unit_Price__c;
                
            }
    
         }
      }

    } 
}