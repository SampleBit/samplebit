Public with sharing class Add_OppProduct{
    
     public List<CProduct__c> pitem{get;set;}
     public List<CProduct__c> pitem2{get;set;}
     public List<COpportunity__c> qitem{get;set;}
     public List<OppProduct__c> qlitem{get;set;}   
     public ID qid {get; set;}
     
    
     public string searchText {get;set;}
     public String message {get;set;}
    public string searchString {get;set;}
     
     public Add_OppProduct(ApexPages.StandardController controller){
         pitem = new List<CProduct__c>();
         pitem2 = new List<CProduct__c>();
         
         qitem = new List<COpportunity__c>();
         qlitem = new List<OppProduct__c>();
        qid = ApexPages.currentPage().getParameters().get('qid');
         
         pitem = [SELECT Name,Id,Company_Name__c,Applicable__c  FROM CProduct__c limit 20];
      
         
         qitem = [SELECT id,Name,CProduct__r.Name FROM COpportunity__c];
         searchText = System.currentPageReference().getParameters().get('lksrch');
         searchString = System.currentPageReference().getParameters().get('lksrch');
     }
     
  
    public PageReference search()
         {
             runSearch(); 
               return null;
          }
     private void runSearch() {
         
             pitem = performSearch(searchString);   
             
          } 

     private List<CProduct__c> performSearch(string searchString) {

         String soql = 'SELECT Name,Id,Company_Name__c,Applicable__c  FROM CProduct__c ';
           if(searchString != '' && searchString != null ) 
           soql = soql +  ' where Name LIKE \'%' + searchString +'%\'';
           soql = soql + ' limit 25';
           System.debug(soql);
           return database.query(soql); 

        }
    public string getFormTag() {
         return System.currentPageReference().getParameters().get('frm');
      }

    
    Public  PageReference csave()
    {
       qid = ApexPages.currentPage().getParameters().get('qid');
       try 
       {
          for(CProduct__c p:pitem)
          {
              OppProduct__c q = new OppProduct__c();
              if(p.Applicable__c==TRUE)
              {
                   
                   q.COpportunity__c =qid; 
                   q.Name =p.Name;
                   q.Company_Name__c=p.Company_Name__c;
                   
                   
                   qlitem.add(q); 
                  
              }
            
          }
          
           system.debug('&&&&'+qid);
           Insert qlitem;
           message = 'updated records Sucessfully';
        } 
        Catch (DMLException e) 
        {
            ApexPages.addMessages(e);
            return null;
        }
        
        //return new PageReference('javascript:self.close()');
        return new PageReference('/'+qid);
   }
   public PageReference cancel() 
   {
    return new PageReference('/'+ApexPages.currentPage().getParameters().get('qid'));
   }
}