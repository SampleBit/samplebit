global class EmailAttachment
{
  public static blob b;

    WebService static string EmailAttachment(string id)
    {
     try
     {
              
          Account qt=[Select id,name from Account where id=:id];
        
        
            PageReference pdf =new PageReference('/apex/DocPDF?='+id);
            
            pdf.setRedirect(true);
           
          //  if(Test.isRunningTest() == false) 
                b = pdf.getContent();
        
        //--------create a mail object to send a single email.--------
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         
         string[] toaddress =String.valueOf(qt.EmailID__c).Split(',');
            
            mail.setToAddresses(toaddress);
          //  mail.setToAddresses(new string[] {qt.email__c}); // Internal_Email_User_Id__c text fied
          
          /*  mail.setBccAddresses(new String[] {'test@globalnest.com'});
            mail.setBccSender(true);*/
          
            //mail.setSenderDisplayName(qt.Account_Name__c);
            
            mail.setSubject('Account For'+' '+qt.name+' : '+qt.name);
            Doctor__c d=new Doctor__c();
        //------------- Create the email attachment-----------------
        // Invoice_Dispatch_details__c invoice=new Invoice_Dispatch_details__c();
       //   invoice=[select id,Account_Lookup__c,Purchase_Order__c from Invoice_Dispatch_details__c where Challan__c=:id];
          
            
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('Account.pdf');
            efa.setBody(b);
        //-----------------------------------------------------------
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
         
      mail.setPlainTextBody('Hi Accounts Team, Please find the doctor for ');
        //------------------------Attach sales order to a contract--------------------
         Attachment formAttach = new Attachment();
         formAttach.Name = 'Challan'+qt.name+'.pdf';
         formAttach.body = b;
         formAttach.ContentType='application/pdf';
         formAttach.parentId =id;
        // if (!Test.isRunningTest()) 
             insert formAttach;
        
            //send the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail } );
           
            return 'email has been sent';
        }
        catch(exception ex)
        {
           
          string ex1=string.valueof(ex);
          return ex1;
            
        }

    }
}