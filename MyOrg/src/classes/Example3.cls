public class Example3 {
    public String name;
    public Date dueDate;
    public decimal fee;
    public Example3(String name,Date dueDate){
        this.name=name;
        this.dueDate=dueDate;
    }
    public void getData(){
        Decimal latefee=0;
        if(System.today()>dueDate){
            Integer no=Date.today().daysBetween(dueDate);
            if(no<=10){
                latefee=1000;
            }
            else {
                latefee=no*100;
            }
        }
        System.debug('latefee'+latefee);
    }

}