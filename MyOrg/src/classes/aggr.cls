public class aggr {
    public List<AggregateResult>result{set;get;}
    public List<Reportwrap>reportList{set;get;}
    public aggr(){
        reportList=new List<Reportwrap>();
        result=[select stagename,Min(Amount)amt from Opportunity group by stagename];
        for(AggregateResult rs:result){
            Reportwrap rw=new Reportwrap();
            rw.stagename=(String)rs.get('stagename');
            rw.amount=(Decimal)rs.get('Amount');
           reportList.add(rw);
        }
    }
    public class Reportwrap{
        public String stagename{set;get;}
        public Decimal Amount{set;get;}
    }

}