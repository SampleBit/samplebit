public class detail {
 Public Account accRec{get;set;}
 Public id accRecId;

 public detail(ApexPages.StandardController controller) {    

   if(ApexPages.currentPage().getParameters().get('id') != null) {
   
     accRecId = [select id,name from Account where id = :ApexPages.currentPage().getParameters().get('id')].id;
      if(accRecId != null)
         accRec = [select id,name,annualrevenue from account where id =:accRecId];
         
   }
 }

}