public class QuotePDF {
    Public Integer i {set;get;}
    public List<string>dlist{set;get;}
    public List<QuoteProductLineItem__c>flist{set;get;}
    public string quoteId{get;set;}
    public MyQuote__c quotes{get;set;}
    public Set<id>qplitemsid {set;get;}
    public Map<ID,QuoteProductLineItem__c> QpLitem {get;set;}
    public List<QuoteLine__c> qliList{get;set;}
    public List<QLIWrap> wrapperList{get;set;}
    Public List<TotalAmountWrap> TotalAmountWrapList {get;set;}
    Public Map<String, String> totalAmountMap {get;set;}
    public QuotePDF(ApexPages.StandardController controller) {
        i=0;
        dlist=new List<string>();
        quoteId = ApexPages.currentPage().getParameters().get('id');
          flist=new List<QuoteProductLineItem__c>();
        List<String> apiNames =new List<String>();

        quotes = (MyQuote__c) controller.getRecord();
        qplitemsid = new set<id>();
        qliList = new List<QuoteLine__c>();
        wrapperList = new List<QLIWrap>();
        TotalAmountWrapList = new List<TotalAmountWrap>();
        Map<String, List<QLIWrap>> qliWrapMap = new Map<String, List<QLIWrap>>();
        totalAmountMap = new Map<String, String>();
        for(QuoteLine__c qli : [SELECT Id,Name,Product_Name__c,Company_Name__c,MyQuote__c
                                         FROM QuoteLine__c
                                          WHERE MyQuote__c =: quoteId]) {
                                              
                                                  qplitemsid.add(qli.id);
                                             if(qli.Company_Name__c != null){
                                                 for(String st : getcompanyname(qli.Company_Name__c)){  
                                                     
                                                     QLIWrap q = new QLIWrap(st);
                                                      q.pid = qli.id;
                                                     if(!String.isBlank(qli.Product_Name__c))
                                                         q.prodname = qli.Product_Name__c;
                                                     if(!String.isBlank(qli.Name))
                                                         q.qliname = qli.Name;
                                                     
                                                 
                                                     if(qliWrapMap == null || !qliWrapMap.containsKey(st)){
                                                         qliWrapMap.put(st, new List<QLIWrap>());
                                                         qliWrapMap.get(st).add(q);
                                                     }else {
                                                         qliWrapMap.get(st).add(q);
                                                     }
                                                 
                                                 }
                                             }
                                         }
        if(qliWrapMap != null && qliWrapMap.size() > 0){
            for(String s : qliWrapMap.keySet()){
                TotalAmountWrap taw = new TotalAmountWrap();
                taw.companyname = s;
                taw.qliWrapList.addAll(qliWrapMap.get(s));
                for(QLIWrap q:taw.qliWrapList){
                    if(q.prodname!=null || q.prodname!='') {
                        dlist.add(q.prodname);
                        i=1;
                    }
                }
                
                TotalAmountWrapList.add(taw);
            }
        }
        QpLitem = new Map<ID, QuoteProductLineItem__c>();
         if(qplitemsid.size() > 0){
             
              for(QuoteProductLineItem__c Qpli : [select id,Name,QuoteLine__c from QuoteProductLineItem__c where QuoteLine__c IN :qplitemsid ])
               {
                    QpLitem.put(Qpli.QuoteLine__c,Qpli);
                   
                   
                
               }
             flist=[select id,Name,QuoteLine__c from QuoteProductLineItem__c where QuoteLine__c IN :QpLitem.keySet() ];
             
         }
    }
    
    private List<String> getcompanyname(String st){
        return st.split(';');
    }
    private List<String> getcq(String st){
        return st.split(';');
    }
    
    public class TotalAmountWrap{
        Public String companyname {get;set;}
        Public String cq{set;get;}
        Public List<QLIWrap> qliWrapList {get;set;}
        
        public TotalAmountWrap(){
            companyname = cq='';
            
            qliWrapList = new List<QLIWrap>();
        }
    }
    public class QLIWrap {
        public String companyname {get;set;}
        public String prodname {get;set;} 
        public string qliname{set;get;}
        
         public id pid {get;set;}
             
        public QLIWrap(){
            companyname = prodName = qliname='';
            
        }
        public QLIWrap(String st){
            companyname = st;
            prodName =qliname='';
            
        }
    }
    
    


}