public class Dochome {
    public list<Doctor__c> dc{set;get;}
  public list<Doctor__c> dc1{set;get;}
     public list<Doctor__c> dc2{set;get;}
     public list<Doctor__c> dc3{set;get;}
    public string selected{set;get;}
    public list<Doctor__c> sdoc{set;get;}
    public list<Doctor__c> doclist{set;get;}

    public Dochome(ApexPages.StandardController controller){
        doclist=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c order by Name limit 100];
        dc=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c];
        dc1=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Type__C='full time Doctor'];
        dc2=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Type__C='Refferal Doctor'];
        dc3=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Type__C='visiting Doctor'];
    }
    public pagereference newdoc(){
        pagereference p=new pagereference('/apex/doctor');
        return p;
    }
    
    public void search(){
        dc=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Name=:selected];
        dc1=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Name=:selected];
        dc2=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Name=:selected];
        dc3=[select Name,Last_Name__c,Phone__c,Email__c,Specialist__c,Type__c,Grade__c from Doctor__c where Name=:selected];
    }
     public void referral(){
        dc1=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Referral' order by Name ASC];
        dc2=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Visit' order by Name ASC];
        dc3=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Full-Time' order by Name ASC];
    }
    public void referral1(){
        dc1=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Referral' order by Name DESC];
        dc2=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Visit' order by Name DESC];
        dc3=[select id,name,Desgination__c,Grade__c,Type__c from Doctor__C where Type__c='Full-Time' order by Name DESC];   
    }
}