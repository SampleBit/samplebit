public class form {
    public List<String>names{set;get;}
    public Set<String>nscollege{set;get;}
    public Set<String>scollege{set;get;}
    public List<selectoption>nsoptions{set;get;}
    public List<selectoption>soptions{set;get;}
    public List<String>selected{set;get;}
    public List<String>removed{set;get;}
    public form(){
        names=new List<String>{'RVR','Vignan','ANU','BEC'};
            nscollege=new Set<String>();
        scollege=new Set<String>();
        nsoptions=new List<selectoption>();
        soptions=new List<selectoption>();
        selected=new List<String>();
        removed=new List<String>();
        nscollege.addAll(names);
        getdata();
    }
    public void getdata(){
        nsoptions.clear();
        soptions.clear();
        for(String s1:nscollege){
            selectoption op1=new selectoption(s1,s1);
            nsoptions.add(op1);
        }
        for(String s2:scollege){
            selectoption op2=new selectoption(s2,s2);
            soptions.add(op2);
        }
    }
    public void add(){
       
        nscollege.removeAll(selected);
        scollege.addAll(selected);
        getdata();
    }
    public void rem(){
      scollege.removeAll(removed);
        nscollege.addAll(removed);
        getdata();
    }
}