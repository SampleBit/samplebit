global class InvoiceEmail
{
public static blob b;

    WebService static string InvoiceEmail(string id)
    {
         try
     {
              
          COrder__c qt=[Select id,name,Invoice_Number__c from COrder__c where id=:id];
        PageReference pdf =new PageReference('/apex/DocPDF?='+id);
            
            pdf.setRedirect(true);
           
           // if(Test.isRunningTest() == false)
                b = pdf.getContent();
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         string[] toaddress =String.valueOf(User.Email).Split(',');
         mail.setToAddresses(toaddress);
         mail.setSubject('Invoice For'+' '+qt.Name+' : '+qt.Invoice_Number__c);
         Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('Invoice.pdf');
            efa.setBody(b);
         mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
         
         mail.setPlainTextBody('Hi Accounts Team, Please find the Invoice for ');
         Attachment formAttach = new Attachment();
         formAttach.Name = 'Invoice'+qt.Name+'.pdf';
         formAttach.body = b;
         formAttach.ContentType='application/pdf';
         formAttach.parentId =id;
       //  if (!Test.isRunningTest())
             insert formAttach;
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail } );
         return 'Invoice Sent';
         }
        catch(exception ex)
        {
           
          string ex1=string.valueof(ex);
          return ex1;
            
        }

    }
    
         
}