public class HRecordType {
    public ID selected {set;get;}
    Patient__c p;
    ApexPages.StandardController controller;
    public HRecordType (ApexPages.StandardController controller) {
        this.controller = controller;
        p = (Patient__c) Controller.getRecord();
    }
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Patient__c' ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
            } 
        }
        return options;
    }
    public pageReference newpage(){
        if(selected=='01228000000ANrc'){
            pageReference Inp= new pageReference('/apex/InPatient') ;
            return inp;
        }else {
            pageReference Out= new pageReference('/apex/OutPatient') ;
             
            return out;
            }
    }
}