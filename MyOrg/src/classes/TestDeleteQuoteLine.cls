@isTest
public class TestDeleteQuoteLine {
    public static testMethod void mytest(){
        MyQuote__c q=new MyQuote__c();
        q.Name='test';
        q.Status__c='Lost';
        insert q;
        update q;
        QuoteLine__c qli=new QuoteLine__c();
        qli.MyQuote__c=q.id;
        insert qli;
        List<QuoteLine__c> qlilist=[select id,MyQuote__c from QuoteLine__c where MyQuote__c=:q.id];
       Test.startTest();
        if(qlilist.size()>0){
            delete qlilist;
        }
       Test.stopTest(); 
        
    }
}