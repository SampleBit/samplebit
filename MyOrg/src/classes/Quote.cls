public class Quote {
   public string quoteId{get;set;}
    public MyQuote__c quotes{get;set;}
    public List<QuoteLine__c> qliList{get;set;}
    public List<QLIWrap>wrapperList{get;set;}
    Public List<TotalAmountWrap> TotalAmountWrapList {get;set;}
    Public Map<String, Double> totalAmountMap {get;set;}
    public Quote(ApexPages.StandardController controller) {
        quoteId = ApexPages.currentPage().getParameters().get('id');
        
        List<String> apiNames =new List<String>();
        /*if(!Test.isRunningTest()){
            Schema.DescribeSObjectResult r = MyQuote__c.sObjectType.getDescribe();
            
            for(string apiName : r.fields.getMap().keySet()){
                apiNames.add(apiName);
            }
            controller.addFields(apiNames);
       }*/
        quotes = (MyQuote__c) controller.getRecord();
        
        qliList = new List<QuoteLine__c>();
        wrapperList = new List<QLIWrap>();
        TotalAmountWrapList = new List<TotalAmountWrap>();
        Map<String, List<QLIWrap>> qliWrapMap = new Map<String, List<QLIWrap>>();
       totalAmountMap = new Map<String, Double>();
        for(QuoteLine__c qli : [SELECT Id, Name,Product_Name__c,Company_Name__c,MyQuote__c
                                          FROM QuoteLine__c
                                          WHERE MyQuote__c =: quoteId
                                         ]) {
                                             if(qli.Company_Name__c != null){
                                                 for(String st : getCompanyName(qli.Company_Name__c)){  
                                                     
                                                     QLIWrap q = new QLIWrap(st);
                                                     if(!String.isBlank(qli.Product_Name__c))
                                                         q.ProdName = qli.Product_Name__c;
                                                     if(!String.isBlank(qli.Company_Name__c))
                                                         q.CompanyName = qli.Company_Name__c;
                                                     
                                                     
                                                     
                                                     if(qliWrapMap == null || !qliWrapMap.containsKey(st)){
                                                         qliWrapMap.put(st, new List<QLIWrap>());
                                                         qliWrapMap.get(st).add(q);
                                                     }else {
                                                         qliWrapMap.get(st).add(q);
                                                     }
                                                 }
                                                 }
                                             
                                         }
        if(qliWrapMap != null && qliWrapMap.size() > 0){
            for(String s : qliWrapMap.keySet()){
                TotalAmountWrap taw = new TotalAmountWrap();
                taw.CompanyName = s;
                taw.qliWrapList.addAll(qliWrapMap.get(s));
                for(QLIWrap q : qliWrapMap.get(s))
                    
            	TotalAmountWrapList.add(taw);
            }
        } 
    }
    
    private List<String>getCompanyName(String st){
        return st.split(';');
    }
    
    public class TotalAmountWrap{
        Public String CompanyName {get;set;}
        Public String ProdName{set;get;}
        Public List<QLIWrap> qliWrapList {get;set;}
        
        public TotalAmountWrap(){
            CompanyName = '';
           
            qliWrapList = new List<QLIWrap>();
        }
    }
    public class QLIWrap {
        public String CompanyName {get;set;}
        public String ProdName {get;set;}
        
        public QLIWrap(){
            CompanyName = ProdName = '';
            
        }
        public QLIWrap(String st){
            CompanyName = st;
            prodName =  '';
           
        }
    }
}