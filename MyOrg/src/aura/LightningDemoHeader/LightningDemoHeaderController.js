({
	updateSampleRecord : function(component, event) {
       
        var recordId = component.get("v.recordId");

        var evt = $A.get("e.c:updateRecordEvent");

        evt.setParams({
            "recordId": recordId
        });

        
        evt.fire();
	}
})