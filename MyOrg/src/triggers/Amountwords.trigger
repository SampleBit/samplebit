trigger Amountwords on CProduct__c (before insert, before update) {
    for (CProduct__c c : Trigger.new) {
        if (c.Price_Block__c != null && c.Price_Block__c >= 0) {
            Long n = c.Price_Block__c.longValue();
            c.Amount_in_words__c = AmountInwords.english_number(n);
        } else {
            c.Amount_in_words__c = null;
        }
    }
}