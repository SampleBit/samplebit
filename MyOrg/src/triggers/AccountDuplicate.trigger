trigger AccountDuplicate on Account (before insert) {
    set<String>names=new set<String>();
    set<String>Duplicate=new set<String>();
    for(Account a:Trigger.New){
        names.add(a.name);
    }
    List<Account>accs=[select id,name from Account where name in:names];
    if(accs.size()>0){
        for(Account ac:accs){
            Duplicate.add(ac.name);
        } 
        for(Account newacc:Trigger.new){
            if(Duplicate.contains(newacc.name))
                newacc.addError('Record already exist');
        }
    }

}