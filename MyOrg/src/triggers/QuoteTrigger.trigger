trigger QuoteTrigger on MyQuote__c (after update) {
    if(trigger.isUpdate && trigger.isAfter){
        List<id> ids = new list<id>();
        for(MyQuote__c newquote:trigger.new){
            MyQuote__c oldquote = trigger.oldMap.get(newquote.Id);
            boolean check = newquote.Check__c == 'Confirm' && oldquote.Check__c != 'Confirm';
            if(check){
                ids.add(newquote.id);
            }                
        }
        
        List<COrder__c> Orders = new List<COrder__c>();
        list<OrderProduct__c> OrderProducts = new list<OrderProduct__c>();
        if(ids.size()>0){
            system.debug('Test 1');
            list<QuoteLine__c> quotelines = [SELECT Id,Name,MyQuote__c,Company_Name__c,Product_Name__c FROM QuoteLine__c WHERE MyQuote__c IN: ids];
            Map<String, Id> PrincipalNames = new Map<String, Id>();  
            if(quotelines.size()>0){
                system.debug('Test 2');
                
                for(QuoteLine__c quoteline:quotelines){
                    if(quoteline.Company_Name__c != null || quoteline.Company_Name__c != ''){
                        system.debug('Test 3');
                        PrincipalNames.put(quoteline.Company_Name__c, quoteline.id);
                    }
                }
            }
            
            list<QuoteLine__c> quotelines2 = [SELECT Id,Name,MyQuote__c,Company_Name__c,Product_Name__c FROM QuoteLine__c WHERE Id IN: PrincipalNames.values()];
            if(quotelines2.size()>0){
                system.debug('Test 4');
                
                for(QuoteLine__c quoteline2:quotelines2){
                    COrder__c Order = new COrder__c();
                    Order.Company_Name__c = quoteline2.Company_Name__c;
                    Order.MyQuote__c = quoteline2.MyQuote__c;
                    Orders.add(Order);
                }
            }
            if(Orders.size()>0){
                insert Orders;
                for(COrder__c Order: Orders){
                    for(QuoteLine__c quoteline:quotelines){
                        if(quoteline.Company_Name__c != null || quoteline.Company_Name__c != ''){
                            if(Order.Company_Name__c == quoteline.Company_Name__c){
                                OrderProduct__c OrderProduct = new OrderProduct__c();
                                OrderProduct.Order__c=Order.id;
                                OrderProduct.Name=quoteline.Product_Name__c;
                                OrderProduct.Company_Name__c=quoteline.Company_Name__c;
                                OrderProducts.add(OrderProduct);
                            }
                        }
                    }  
                }
            }
            if(OrderProducts.size()>0){
                insert OrderProducts;
            }
        }
    }
}