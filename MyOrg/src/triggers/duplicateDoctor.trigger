trigger duplicateDoctor on Doctor__c (before insert, before update) {
set<String> Nameset= new set<String>();
   set<String> Specialistset= new set<String>();
   for(Doctor__c doc: Trigger.new){
       if(doc.Name!=null && doc.Name!=''){
       Nameset.add(doc.Name);
       }
       if(doc.Specialist__c!=null && doc.Specialist__c!= ''){
       Specialistset.add(doc.Specialist__c);
       }
   }
   String queryString= 'select Name, Specialist__c from Doctor__c where Specialist__c IN : Specialistset AND Name IN: Nameset';
   /*if(Nameset.size()>0){
       queryString= queryString + 'AND Name IN: Nameset';
   }*/
   List<Doctor__C> existingdoclist = new List<Doctor__C>();
   existingdoclist= database.query(queryString);
   if(existingdoclist.size()>0){
       for(Doctor__c newdoc : Trigger.new){
       for(Doctor__c existingdoc : existingdoclist ){
           if((newdoc.Name == existingdoc.Name)&&(newdoc.Specialist__c==existingdoc.Specialist__c)){
               newdoc.addError('Duplicate Doctor');
               }
           }
       }
   }
}