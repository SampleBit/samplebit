/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/savePDF/*')
global class QuotePDF_Create {
    global QuotePDF_Create() {

    }
    @HttpPost
    global static void savePDF(List<String> sObj_template_id) {

    }
}
